import Vue from 'vue';
import App from './App.vue';
import { Pagination, Image, Row, Col } from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import "element-ui/lib/theme-chalk/index.css";

locale.use(lang);
Vue.use(Pagination);
Vue.use(Image);
Vue.use(Row);
Vue.use(Col);

new Vue({
  el: '#app',
  render: h => h(App)
})
